package com.example.core.extension

import com.example.core.network.Mapper

fun <A, B> A.mapTo(mapper: Mapper<A, B>): B {
    return mapper.to(this)
}