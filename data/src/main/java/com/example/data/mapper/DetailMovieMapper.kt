package com.example.data.mapper

import com.example.core.network.Mapper
import com.example.data.model.ResponseDetailMovie
import com.example.domain.model.DetailMovie

class DetailMovieMapper: Mapper<ResponseDetailMovie, DetailMovie> {
    override fun to(t: ResponseDetailMovie): DetailMovie {
        return DetailMovie(
            plot = t.plot,
            runtime = t.runtime
        )
    }
}
