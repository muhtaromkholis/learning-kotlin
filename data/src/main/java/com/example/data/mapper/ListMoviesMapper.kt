package com.example.data.mapper

import com.example.core.network.Mapper
import com.example.data.model.SearchItem
import com.example.domain.model.MovieItem

class ListMoviesMapper: Mapper<List<SearchItem>, List<MovieItem>> {
    override fun to(t: List<SearchItem>): List<MovieItem> {
        return t.map {
            MovieItem(
                title = it.title,
                poster = it.poster,
                year = it.year,
                imdbID = it.imdbID,
                type = it.type
            )
        }
    }
}