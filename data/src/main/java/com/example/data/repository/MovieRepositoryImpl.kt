package com.example.data.repository

import com.example.core.data.Resource
import com.example.core.extension.buildNetwork
import com.example.core.extension.mapTo
import com.example.core.network.Mapper
import com.example.data.mapper.DetailMovieMapper
import com.example.data.mapper.ListMoviesMapper
import com.example.data.safeApiCall
import com.example.data.service.MovieService
import com.example.domain.model.DetailMovie
import com.example.domain.model.MovieItem
import com.example.domain.repository.MovieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class MovieRepositoryImpl(
    private val service: MovieService,
    private val listMoviesMapper: ListMoviesMapper,
    private val detailMovieMapper: DetailMovieMapper
): MovieRepository {
    override suspend fun getListMovies(s: String?): Flow<Resource<List<MovieItem>>> {
        return flow {
            val response = safeApiCall(Dispatchers.IO) {
                service.getListMovies(s, 1).search!!.mapTo(listMoviesMapper)
            }
            emit(response)
        }.buildNetwork()
    }

    override suspend fun getDetailMovie(s: String?): Flow<Resource<DetailMovie>> {
        return flow {
            val response = safeApiCall(Dispatchers.IO) {
                service.getDetailMovie(s).mapTo(detailMovieMapper)
            }
            emit(response)
        }.buildNetwork()
    }
}
