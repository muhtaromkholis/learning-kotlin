package com.example.data.service

import com.example.data.model.ResponseDetailMovie
import com.example.data.model.ResponseListMovies
import com.example.data.model.SearchItem
import com.example.data.model.base.BaseResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface MovieService {
    @GET(".")
    suspend fun getListMovies(
        @Query("s") s: String?,
        @Query("page") page: Int
    ): BaseResponse<List<SearchItem>>

    @GET(".")
    suspend fun getDetailMovie(
        @Query("i") i: String?
    ): ResponseDetailMovie
}