package com.example.data.model.base

import com.example.data.model.SearchItem
import com.google.gson.annotations.SerializedName

open class BaseResponse<T> {
    @field:SerializedName("Response")
    val response: String? = null

    @field:SerializedName("totalResults")
    val totalResults: String? = null

    @field:SerializedName("Search")
    val search: T? = null
}