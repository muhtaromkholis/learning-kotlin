package com.example.domain.usecase

import com.example.core.data.Resource
import com.example.core.network.FlowUseCase
import com.example.domain.model.DetailMovie
import com.example.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DetailMovieUseCase(private val repo: MovieRepository): FlowUseCase<String?, DetailMovie>() {
    override suspend fun execute(s: String?): Flow<Resource<DetailMovie>> {
        return s?.let { repo.getDetailMovie(it) }
            ?: flow { Resource.Error(Throwable("Body request can't be empty")) }
    }
}