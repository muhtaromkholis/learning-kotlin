package com.example.domain.usecase

import com.example.core.data.Resource
import com.example.core.network.FlowUseCase
import com.example.domain.model.MovieItem
import com.example.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class ListMoviesUseCase(private val repo: MovieRepository): FlowUseCase<String?, List<MovieItem>>() {
    override suspend fun execute(s: String?): Flow<Resource<List<MovieItem>>> {
        return s?.let { repo.getListMovies(it) }
            ?: flow { Resource.Error(Throwable("Body request can't be empty")) }
    }
}