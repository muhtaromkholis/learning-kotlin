package com.example.domain.repository

import com.example.core.data.Resource
import com.example.domain.model.DetailMovie
import com.example.domain.model.MovieItem
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    suspend fun getListMovies(s: String?): Flow<Resource<List<MovieItem>>>

    suspend fun getDetailMovie(s: String?): Flow<Resource<DetailMovie>>
}