package com.example.domain.model

data class DetailMovie (
    var plot: String? = null,
    var runtime: String? = null
)