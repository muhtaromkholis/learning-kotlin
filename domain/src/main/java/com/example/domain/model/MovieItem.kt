package com.example.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

data class MovieItem(
    var title: String? = null,
    var poster: String? = null,
    var year: String? = null,
    var imdbID: String? = null,
    var type: String? = null
)
