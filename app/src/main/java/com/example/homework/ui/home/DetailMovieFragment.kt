package com.example.homework.ui.home

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.core.data.Resource
import com.example.core.extension.observeData
import com.example.core.extension.toast
import com.example.homework.R
import com.example.homework.databinding.FragmentDetailMovieBinding
import com.example.homework.ui.utils.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailMovieFragment : Fragment(R.layout.fragment_detail_movie) {
    private val binding by viewBinding(FragmentDetailMovieBinding::bind)
    private val detailMovieViewModel: DetailMovieViewModel by viewModel()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        observeDetailMovie()
    }

    private fun init() {
        val args = DetailMovieFragmentArgs.fromBundle(arguments as Bundle)
        detailMovieViewModel.getDetailMovie(args.movieID)
        setDetailMovie(args)
    }

    private fun setDetailMovie(data: DetailMovieFragmentArgs) {
        binding.apply {
            Glide
                .with(this@DetailMovieFragment)
                .load(data.poster)
                .into(ivPoster)
            tvTitle.text = data.title
            tvYear.text = data.year
            tvLorem1.text = "   ${resources.getString(R.string.lorem_ipsum)}"
            tvLorem2.text = "   ${resources.getString(R.string.lorem_ipsum)}"
        }

        val actionBar: ActionBar? = (activity as? AppCompatActivity)?.supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.title = data.title
    }

    private fun observeDetailMovie() {
        observeData(detailMovieViewModel.detailMovie) { result ->
            result?.let {
                when(it) {
                    is Resource.Success -> {
                        it.model?.let {data ->
                            binding.apply {
                                tvRuntime.text = data.runtime
                                tvPlot.text = data.plot
                            }
                        }
                    }
                    is Resource.Error -> toast("${it.error.message}")
                    else -> {}
                }
            }
        }
    }
}