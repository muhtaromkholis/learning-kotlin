package com.example.homework.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.core.data.Resource
import com.example.domain.model.MovieItem
import com.example.domain.usecase.ListMoviesUseCase
import kotlinx.coroutines.launch

class HomeViewModel(private val listMoviesUseCase: ListMoviesUseCase) : ViewModel() {
    private val _dataListNewReleaseMovies = MutableLiveData<Resource<List<MovieItem>>>()
    val dataListNewReleaseMovies: LiveData<Resource<List<MovieItem>>> = _dataListNewReleaseMovies

    private val _dataListFeaturedMovies = MutableLiveData<Resource<List<MovieItem>>>()
    val dataListFeaturedMovies: LiveData<Resource<List<MovieItem>>> = _dataListFeaturedMovies

    fun getListNewReleaseMovies(title: String?) {
        viewModelScope.launch {
            listMoviesUseCase(title).collect{
                _dataListNewReleaseMovies.value = it
            }
        }
    }

    fun getListFeaturedMovies(title: String) {
        viewModelScope.launch {
            listMoviesUseCase(title).collect{
                _dataListFeaturedMovies.value = it
            }
        }
    }
}