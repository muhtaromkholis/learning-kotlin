package com.example.homework.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.core.data.Resource
import com.example.domain.model.DetailMovie
import com.example.domain.usecase.DetailMovieUseCase
import kotlinx.coroutines.launch

class DetailMovieViewModel(
    private val detailMovieUseCase: DetailMovieUseCase
) : ViewModel() {

    private val _detailMovie = MutableLiveData<Resource<DetailMovie>>()
    val detailMovie: LiveData<Resource<DetailMovie>> = _detailMovie

    fun getDetailMovie(title: String?) {
        viewModelScope.launch {
            detailMovieUseCase(title).collect{
                _detailMovie.value = it
            }
        }
    }
}