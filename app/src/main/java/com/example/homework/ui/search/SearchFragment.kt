package com.example.homework.ui.search

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.core.data.Resource
import com.example.core.extension.observeData
import com.example.core.extension.toast
import com.example.homework.R
import com.example.homework.databinding.FragmentSearchBinding
import com.example.homework.ui.home.SearchListAdapter
import com.example.homework.ui.utils.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : Fragment(R.layout.fragment_search) {
    private val binding by viewBinding(FragmentSearchBinding::bind)
    private val searchViewModel: SearchViewModel by viewModel()
    private val searchListAdapter by lazy { SearchListAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar(false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSearchView()
        setupRv()
        observeSearchList()
    }

    private fun setupActionBar(params: Boolean) {
        val actionBar: ActionBar? = (activity as? AppCompatActivity)?.supportActionBar
        if (params) {
            actionBar?.show()
        } else {
            actionBar?.hide()
        }
    }

    private fun setupSearchView() {
        binding.apply {
            searchView.setupWithSearchBar(searchBar)
            searchView.editText.setOnEditorActionListener{ textView, i, keyEvent ->
                searchViewModel.searchMovie(searchView.text.toString())
                false
            }
        }
    }

    private fun setupRv() {
        val layoutManagerSearchList = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.rvSearchMovie.apply {
            layoutManager = layoutManagerSearchList
            addItemDecoration(DividerItemDecoration(activity, layoutManagerSearchList.orientation))
            adapter = searchListAdapter
        }
    }

    private fun observeSearchList() {
        observeData(searchViewModel.dataSearchMovie) { result ->
            result?.let {
                when(it) {
                    is Resource.Success -> {
                        it.model?.let {data ->
                            searchListAdapter.submitList(data.toMutableList())
                        }
                    }
                    is Resource.Error -> toast("${it.error.message}")
                    else -> {}
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        setupActionBar(true)
    }

    override fun onResume() {
        super.onResume()
        setupActionBar(false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        setupActionBar(true)
    }
}