package com.example.homework.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.core.data.Resource
import com.example.domain.model.MovieItem
import com.example.domain.usecase.ListMoviesUseCase
import kotlinx.coroutines.launch

class SearchViewModel(private val listMoviesUseCase: ListMoviesUseCase) : ViewModel() {
    private val _dataSearchMovie = MutableLiveData<Resource<List<MovieItem>>>()
    val dataSearchMovie: LiveData<Resource<List<MovieItem>>> = _dataSearchMovie

    fun searchMovie(title: String?) {
        viewModelScope.launch {
            listMoviesUseCase(title).collect{
                _dataSearchMovie.value = it
            }
        }
    }
}