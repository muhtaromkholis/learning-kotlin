package com.example.homework.ui.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.core.data.Resource
import com.example.core.extension.observeData
import com.example.core.extension.toast
import com.example.homework.R
import com.example.homework.databinding.FragmentHomeBinding
import com.example.homework.ui.utils.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment(R.layout.fragment_home) {
    private val binding by viewBinding(FragmentHomeBinding::bind)
    private val homeViewModel: HomeViewModel by viewModel()
    private val newReleaseAdapter by lazy { ListMovieAdapter() }
    private val featuredAdapter by lazy { ListMovieAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setupRv()
        observeListMovies()
    }

    private fun init() {
        homeViewModel.getListNewReleaseMovies("Avengers")
        homeViewModel.getListFeaturedMovies("Star Wars")
    }

    private fun setupRv() {
        val layoutManagerNewRelease = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.rvListNewReleaseMovies.apply {
            layoutManager = layoutManagerNewRelease
            addItemDecoration(DividerItemDecoration(activity, layoutManagerNewRelease.orientation))
            adapter = newReleaseAdapter
        }

        val layoutManagerFeatured = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.rvListFeaturedMovies.apply {
            layoutManager = layoutManagerFeatured
            addItemDecoration(DividerItemDecoration(activity, layoutManagerFeatured.orientation))
            adapter = featuredAdapter
        }
    }

    private fun observeListMovies() {
        observeData(homeViewModel.dataListNewReleaseMovies) { result ->
            result?.let {
                when(it) {
                    is Resource.Success -> {
                        it.model?.let {data ->
                            newReleaseAdapter.submitList(data.toMutableList())
                        }
                    }
                    is Resource.Error -> toast("${it.error.message}")
                    else -> {}
                }
            }
        }

        observeData(homeViewModel.dataListFeaturedMovies) { result ->
            result?.let {
                when(it) {
                    is Resource.Success -> {
                        it.model?.let {data ->
                            featuredAdapter.submitList(data.toMutableList())
                        }
                    }
                    is Resource.Error -> toast("${it.error.message}")
                    else -> {}
                }
            }
        }
    }
}