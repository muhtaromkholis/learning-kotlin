package com.example.homework.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.domain.model.MovieItem
import com.example.homework.databinding.ItemSearchMovieBinding
import com.example.homework.ui.search.SearchFragmentDirections

class SearchListAdapter: ListAdapter<MovieItem, SearchListAdapter.MyViewHolder>(DIFF_CALLBACK) {
    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<MovieItem>() {
            override fun areContentsTheSame(oldItem: MovieItem, newItem: MovieItem): Boolean {
                return oldItem == newItem
            }

            override fun areItemsTheSame(oldItem: MovieItem, newItem: MovieItem): Boolean {
                return oldItem == newItem
            }
        }
    }

    inner class MyViewHolder(val binding: ItemSearchMovieBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(dataMovie: MovieItem) {
            binding.tvTitle.text = dataMovie.title
            binding.tvYear.text = dataMovie.year
            Glide
                .with(itemView.context)
                .load(dataMovie.poster)
                .into(binding.ivPoster)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = ItemSearchMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val dataMovie = getItem(position)
        holder.itemView.setOnClickListener{
            val toDetailMovieFragment = SearchFragmentDirections.actionNavigationSearchToDetailMovieFragment()
            toDetailMovieFragment.movieID = dataMovie.imdbID.toString()
            toDetailMovieFragment.title = dataMovie.title.toString()
            toDetailMovieFragment.poster = dataMovie.poster.toString()
            toDetailMovieFragment.year = dataMovie.year.toString()
            it.findNavController().navigate(toDetailMovieFragment)
        }
        holder.bind(dataMovie)
    }
}