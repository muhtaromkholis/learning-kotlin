package com.example.homework.module

import com.example.data.repository.MovieRepositoryImpl
import com.example.domain.repository.MovieRepository
import org.koin.dsl.module

val repositoryModule = module {
    // Movie
    single<MovieRepository> {
        MovieRepositoryImpl(get(), get(), get())
    }
}