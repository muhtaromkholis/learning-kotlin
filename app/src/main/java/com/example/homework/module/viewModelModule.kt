package com.example.homework.module

import com.example.homework.ui.home.DetailMovieViewModel
import com.example.homework.ui.home.HomeViewModel
import com.example.homework.ui.search.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    // Movie
    viewModel { HomeViewModel(get()) }
    viewModel { DetailMovieViewModel(get()) }
    viewModel { SearchViewModel(get()) }
}