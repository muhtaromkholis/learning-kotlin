package com.example.homework.module

import com.example.domain.usecase.DetailMovieUseCase
import com.example.domain.usecase.ListMoviesUseCase
import org.koin.dsl.module

val useCaseModule = module {
    // Movie
    single { ListMoviesUseCase(get()) }
    single { DetailMovieUseCase(get()) }
}