package com.example.homework.module

val appModule = listOf(
    viewModelModule,
    repositoryModule,
    useCaseModule,
    networkModule,
    mapperModule,
    serviceModule
)