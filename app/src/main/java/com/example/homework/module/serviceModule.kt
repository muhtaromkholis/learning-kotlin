package com.example.homework.module

import com.example.data.service.MovieService
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val serviceModule = module {
    factory { get<Retrofit>(qualifier = named("MOVIE_BASE_URL")).create(MovieService::class.java) }
}