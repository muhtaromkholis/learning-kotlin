package com.example.homework.module

import android.content.Context
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    val cacheSize = (10 * 1024 * 1024).toLong() // 10mb
    val READ_TIMEOUT = 180.toLong() // 3 min
    val CONNECT_TIMEOUT = 180.toLong() // 3 min

    single { GsonBuilder().create() }
    single {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        OkHttpClient.Builder().apply {
            cache(Cache(androidContext().cacheDir, cacheSize))
            readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            addInterceptor(logging)
                .addInterceptor(object : Interceptor {
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val originalRequest = chain.request()
                        val newRequest = originalRequest.newBuilder()
                            .header("X-RapidAPI-Key", "0d506939cemsh014920d5939c43bp12de22jsn8ec3091c6daf")
                            .header("X-RapidAPI-Host", "movie-database-alternative.p.rapidapi.com")
                            .build()
                        return chain.proceed(newRequest)
                    }
                })
        }.build()
    }

    single<Retrofit>(named("MOVIE_BASE_URL")) {
        Retrofit.Builder()
            .baseUrl("https://movie-database-alternative.p.rapidapi.com/")
            .addConverterFactory(GsonConverterFactory.create(get()))
            .client(get())
            .build()
    }
}