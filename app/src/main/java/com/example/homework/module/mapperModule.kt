package com.example.homework.module

import com.example.data.mapper.DetailMovieMapper
import com.example.data.mapper.ListMoviesMapper
import org.koin.dsl.module

val mapperModule = module {
    factory { ListMoviesMapper() }
    factory { DetailMovieMapper() }
}